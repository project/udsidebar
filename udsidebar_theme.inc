<?php

/**
 * @file
 * Provides theme functions for the module.
 */

/**
 * Custom theme function: box
 */
function theme_udsidebar_box($back, $link, $title, $image, $content) {
  $box  = '<div class="udsidebar-item" style="background-image: url(' . $back . '/back.jpg);">';
  $box .= '  <a href="' . $link . '"><img src="' . $image . '" alt="' . $title . '" /></a>';
  $box .= '  <a href="' . $link . '"><strong>' . $title . '</strong></a>';
  $box .= '  <p>' . $content . '</p>';
  $box .= '</div>';

  return $box;
}

