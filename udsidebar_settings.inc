<?php

/**
 * @file
 * Module Settings
 */

/**
 * Core settings function.
 */
function udsidebar_admin_settings() {
  $bid = intval(arg(3)); // @deprecated arg()
  if ($bid > 0) {
    $edit = db_fetch_array(db_query('SELECT * FROM {udsidebar} WHERE bid = %d', $bid));
    $output .= drupal_get_form('udsidebar_linkform', $edit, TRUE);
  }
  else {
    $output .= drupal_get_form('udsidebar_linkform', $edit, FALSE);
    $output .= t('<h2>Sidebar Maps</h2>');
    $output .= udsidebar_view_list();
  }
  return $output;
}

/**
 * Build form to add/edit location links
 */
function udsidebar_linkform(&$form_state, $edit = array(), $individual = FALSE) {

  // Get images
  $dir =  drupal_get_path('module', 'udsidebar') . "/images/";
  $handle = opendir($dir);
  while (false !== ($file = readdir($handle))) {
    if ($file != '.' && $file != '..') {
      $images[$file] = $file;
    }
  }
  closedir($handle);

  $form = array();

  $c = $edit['bid'] > 0 ? FALSE : TRUE;
  $t = $edit['bid'] > 0 ? 'Edit' : 'Add';
  $form['udsidebar'] = array(
    '#type' => 'fieldset',
    '#title' => t($t . ' Sidebar'),
    '#collapsible' => $c,
    '#collapsed' => $c,
    '#tree' => FALSE,
  );
  $form['udsidebar']['bid'] = array(
    '#type' => 'hidden',
    '#value' => $edit['bid']
  );
  $form['udsidebar']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Box Title'),
    '#default_value' => t($edit['title']),
    '#description' => t('Give the box a title, this will appear as the top of the text'),
  );
  $form['udsidebar']['image'] = array(
    '#type' => 'select',
    '#title' => t('Box Image'),
    '#options' => $images,
    '#default_value' => t($edit['image']),
    '#description' => t('Select the image to be used on the side of the sidebar.'),
  );
  $form['udsidebar']['titlelink'] = array(
    '#type' => 'textfield',
    '#title' => t('Box Title Link'),
    '#default_value' => t($edit['title']),
    '#description' => t('This is where clicking the box title or image will bring the user'),
  );
  $form['udsidebar']['position'] = array(
    '#type' => 'textfield',
    '#title' => t('Box Position'),
    '#default_value' => t($edit['position']),
    '#description' => t('Pick the order object will be displayed.'),
  );
  $form['udsidebar']['content'] = array(
    '#type' => 'textarea',
    '#title' => t('Text to be displayed as the main content'),
    '#default_value' => $edit['content'],
  );
  $form['udsidebar']['submit'] = array(
    '#type' => 'submit',
    '#value' => $edit['bid'] > 0 ? 'Update' : 'Add'
  );

  if ($individual) {
    $form['udsidebar']['delete'] = array(
      '#type' => 'submit',
      '#value' => 'Delete',
    );
  }

  return $form;
}

/**
 * Build list of existing linked locations
 */
function udsidebar_view_list() {
  $maps = db_query('SELECT * FROM {udsidebar}');
  $rows = array();
  $sidebars = array('Title', 'Image', 'Position', 'Edit');

  while ($map = db_fetch_object($maps)) {
    $bid = strval($map->bid);
    array_push($rows, array(
      $map->title,
      $map->image,
      $map->position,
      l(t('edit'), 'admin/settings/udsidebar/'. $bid),
    )
    );
  }
  return theme('table', $sidebars, $rows);
}

/**
 * Add/Delete/Update location link to database
 */
function udsidebar_linkform_submit($form, &$form_state) {

  $form_state['redirect'] = 'admin/settings/udsidebar';

  $op  = $form_state['clicked_button']['#post']['op'];
  $bid = $form_state['clicked_button']['#post']['bid'];
  $title = $form_state['clicked_button']['#post']['title'];
  $image = $form_state['clicked_button']['#post']['image'];
  $titlelink = $form_state['clicked_button']['#post']['titlelink'];
  $position = $form_state['clicked_button']['#post']['position'];
  $content = $form_state['clicked_button']['#post']['content'];

  // Update
  if ($op == 'Update') {
    $rslt = db_query('UPDATE {udsidebar} SET title="%s", image="%s", titlelink="%s", position="%s", content="%s" WHERE bid=%d', $title, $image, $titlelink, $position, $content, $bid);
  }

  // Delete
  if ($op == 'Delete') {
    $rslt = db_query('DELETE FROM {udsidebar} WHERE bid=%d', $bid);
  }

  // Add
  if ($op == 'Add') {
    $rslt = db_query('INSERT INTO {udsidebar} (title, image, titlelink, position, content) VALUES ("%s", "%s", "%s", "%s", "%s")', $title, $image, $titlelink, $position, $content);
  }

  // Check Result
  if ('1' == $rslt) {
    drupal_set_message(t('Record modification made.'));
    return TRUE;
  }
  else {
    trigger_error(t('Record modification failed.'));
    return FALSE;
  }
}

